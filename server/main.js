import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  Pigs = new Mongo.Collection('pigs');
  Pigs.insert([
	{"type": "meat", "price": 14, "createdAt": new Date()},
	{"type": "meat", "price": 15, "createdAt": new Date()},
	{"type": "meat", "price": 18, "createdAt": new Date()},
	{"type": "meat", "price": 19, "createdAt": new Date()},
	{"type": "meat", "price": 18, "createdAt": new Date()},
	{"type": "meat", "price": 15, "createdAt": new Date()},
	{"type": "meat", "price": 14, "createdAt": new Date()},
	{"type": "meat", "price": 15, "createdAt": new Date()},
	{"type": "not-meat", "price": 20, "createdAt": new Date()},
	{"type": "not-meat", "price": 21, "createdAt": new Date()},
	{"type": "not-meat", "price": 24, "createdAt": new Date()},
	{"type": "not-meat", "price": 25, "createdAt": new Date()},
	{"type": "not-meat", "price": 24, "createdAt": new Date()},
	{"type": "not-meat", "price": 21, "createdAt": new Date()},
	{"type": "not-meat", "price": 20, "createdAt": new Date()},
	{"type": "not-meat", "price": 21, "createdAt": new Date()}
	])
});
