Template.register.events({
'click #register-btn': function(e,t){
    var email = t.find('#email').value,
        username = t.find('#username').value,
        password = t.find('#password').value;
    
    Accounts.createUser({username:username,password:password,email:email},function(err){
      if (err !== undefined) {
        Toast.error(err.reason);
      };
    });
}
});