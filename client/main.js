Toast.info("Привіт. Щоб продовжити користуватися сайтом увійдіть під своїм іменем або зареєструйтеся!");

Pigs = new Mongo.Collection('pigs');
Pigs.insert([
	{"type": "meat", "price": 14, "createdAt": new Date()},
	{"type": "meat", "price": 15, "createdAt": new Date()},
	{"type": "meat", "price": 18, "createdAt": new Date()},
	{"type": "meat", "price": 19, "createdAt": new Date()},
	{"type": "meat", "price": 18, "createdAt": new Date()},
	{"type": "meat", "price": 15, "createdAt": new Date()},
	{"type": "meat", "price": 14, "createdAt": new Date()},
	{"type": "meat", "price": 15, "createdAt": new Date()},
	{"type": "not-meat", "price": 20, "createdAt": new Date()},
	{"type": "not-meat", "price": 21, "createdAt": new Date()},
	{"type": "not-meat", "price": 24, "createdAt": new Date()},
	{"type": "not-meat", "price": 25, "createdAt": new Date()},
	{"type": "not-meat", "price": 24, "createdAt": new Date()},
	{"type": "not-meat", "price": 21, "createdAt": new Date()},
	{"type": "not-meat", "price": 20, "createdAt": new Date()},
	{"type": "not-meat", "price": 21, "createdAt": new Date()}
	])
var allPigs = Pigs.find().fetch()[0], 
meatPigsPrice = [], 
notMeatPigsPrice = [];

for (var key in allPigs) {
	if(allPigs[key].type == "meat"){
		meatPigsPrice.push(allPigs[key].price);
	}
	else{
		notMeatPigsPrice.push(allPigs[key].price);
	}
}

import Highcharts from 'highcharts';
Template.content.events({
	'click #logout': function(e,t){
		Meteor.logout();
	}
});
Template.body.onRendered(function(){
	var loginLink = document.getElementById("loginLink"),
	registerLink = document.getElementById("registerLink"),
	loginForm = document.getElementById("loginForm"),
	registerForm = document.getElementById("registerForm");
	
	loginLink.onclick = function(){
		registerForm.style.display = "none";
		loginForm.style.display = "block";
	}
	registerLink.onclick = function(){
		registerForm.style.display = "block";
		loginForm.style.display = "none";
	}
})

Template.content.onRendered(function() {
	document.getElementById("welcome").innerText = "Вітаємо," + Meteor.user().username;

	Highcharts.chart('container', {
	    title: {
	        text: 'Динаміка цін на свиней у живій вазі, 2010 р. - 2017 р'
	    },
	    yAxis: {
	        title: {
	            text: 'грн/кг'
	        }
	    },
	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'middle'
	    },
	    plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	            pointStart: 2010
	        }
	    },
	    series: [
	    {
	    	name: 'м\'ясо-сальні свині',
	    	data: notMeatPigsPrice
	    },
	    {
	        name: 'свині м\'ясного типу',
	        data: meatPigsPrice
	    }],
	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }
	});  
})