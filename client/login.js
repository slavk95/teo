Template.login.events({
    'click #login-button': function(e,t){
        var email = t.find('#login-email').value,
            password = t.find('#login-password').value;
 
        Meteor.loginWithPassword(email,password,function(err){
          if (err !== undefined) {
          	Toast.error(err.reason);
          };
        });
    }
});